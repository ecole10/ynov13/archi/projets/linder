package com.ynov.infra.repository;

import com.ynov.domain.model.Entreprise;
import com.ynov.domain.port.output.EntrepriseStorage;
import com.ynov.infra.mapper.EntrepriseMapper;
import com.ynov.infra.repository.dao.entreprise.EntrepriseDao;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class EntepriseRepository implements EntrepriseStorage, PanacheRepositoryBase<EntrepriseDao, String> {
    @Inject
    EntrepriseMapper entrepriseMapper;
    
    @Override
    @Transactional
    public Entreprise save(Entreprise entreprise) {
        EntrepriseDao entrepriseDao = entrepriseMapper.entrepriseToEntrepriseDao(entreprise);
        persistAndFlush(entrepriseDao);
        return entrepriseMapper.entrepriseDaoToEntreprise(entrepriseDao);
    }
}
