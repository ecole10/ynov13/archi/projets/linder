package com.ynov.infra.controller.dto.request.profil;

import com.ynov.domain.model.offre.TypeOffre;
import com.ynov.infra.controller.dto.request.MotCleRequestDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

import static javax.persistence.EnumType.STRING;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateProfilRequestDto {
    @NotEmpty(message = "Firstname can't be empty")
    private String prenom;

    @NotEmpty(message = "Lastname can't be empty")
    private String nom;

    @NotEmpty(message = "Picture can't be empty")
    private String photo;

    @Email
    @NotEmpty(message = "Email can't be empty")
    private String email;

    @Size(max = 280, message = "Description max size is 280 characters")
    private String description;

    private List<MotCleRequestDto> motsCles;

    private double salaireHoraireNetMinimum;

    @NotNull
    @Enumerated(STRING)
    private TypeOffre offre;
}
