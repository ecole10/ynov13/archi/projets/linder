package com.ynov.domain.model;

import java.util.Date;

public record Entreprise(String nom, String siren, String description, String logo, Date dateInscription) {

}
