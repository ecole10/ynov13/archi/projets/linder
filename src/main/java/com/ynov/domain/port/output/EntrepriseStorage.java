package com.ynov.domain.port.output;

import com.ynov.domain.model.Entreprise;

public interface EntrepriseStorage {
    Entreprise save(Entreprise entreprise);
}
