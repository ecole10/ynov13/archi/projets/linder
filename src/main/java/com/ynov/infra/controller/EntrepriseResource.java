package com.ynov.infra.controller;

import com.ynov.domain.model.Entreprise;
import com.ynov.domain.port.input.entreprise.CreateEnterprise;
import com.ynov.infra.controller.dto.request.entreprise.CreateEntrepriseRequestDto;
import com.ynov.infra.controller.dto.response.entreprise.CreateEntrepriseResponseDto;
import com.ynov.infra.mapper.EntrepriseMapper;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.ynov.domain.port.output.EntrepriseStorage;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import static javax.ws.rs.core.Response.Status.CREATED;

@Tag(name = "Entreprise")
@Path("entreprises")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EntrepriseResource {
    @Inject
    EntrepriseMapper entrepriseMapper;

    @Inject
    EntrepriseStorage entrepriseStorage;

    @POST
    @APIResponses(value = {
            @APIResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = CreateEntrepriseResponseDto.class))),
            @APIResponse(responseCode = "400", content = @Content(example = "Name can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "SIREN can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "A valid SIREN is composed of 9 digits")),
            @APIResponse(responseCode = "400", content = @Content(example = "Description max size is 280 characters")),
            @APIResponse(responseCode = "400", content = @Content(example = "Logo url can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "SIREN already exist")),
    })
    public Response createEntreprise(@Valid CreateEntrepriseRequestDto request) {
        CreateEnterprise createEnterprise = new CreateEnterprise(entrepriseStorage);

        Entreprise newEntreprise = entrepriseMapper.createEntrepriseRequestDtoToEntreprise(request);
        Entreprise entreprise = createEnterprise.create(newEntreprise);

        return Response.status(CREATED)
                .entity(entrepriseMapper.entrepriseToCreateEntrepriseResponseDto(entreprise))
                .build();
    }
}
