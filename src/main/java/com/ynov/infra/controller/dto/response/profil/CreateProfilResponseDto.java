package com.ynov.infra.controller.dto.response.profil;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateProfilResponseDto {
    private String id;
}
