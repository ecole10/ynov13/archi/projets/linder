package com.ynov.infra.repository.dao.offre;

import com.ynov.domain.model.offre.StatutOffre;
import com.ynov.domain.model.offre.TypeOffre;
import com.ynov.infra.repository.dao.MotCleDao;
import com.ynov.infra.repository.dao.entreprise.EntrepriseDao;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

import java.util.List;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Entity(name = "offre")
@Getter
@Setter
@NoArgsConstructor
public class OffreDao {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Length(max = 280)
    @Column(name = "description")
    private String description;

    @Column(name = "salaire")
    private float salaire;

    @ManyToMany
    @JoinTable(
            name = "offre_mot_cle",
            joinColumns = @JoinColumn(name = "offre_id"),
            inverseJoinColumns = @JoinColumn(name = "mot_cle_valeur")
    )
    private List<MotCleDao> motsCles;

    @Enumerated(STRING)
    @Column(name = "type")
    private TypeOffre type;

    @Enumerated(STRING)
    @Column(name = "statut")
    private StatutOffre statut;

    @Column(name = "accessible_recruteur")
    private boolean accessibleRecruteur;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "entreprise_siren")
    private EntrepriseDao entreprise;
}
