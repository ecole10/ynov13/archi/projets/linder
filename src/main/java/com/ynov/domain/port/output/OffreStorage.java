package com.ynov.domain.port.output;

import com.ynov.domain.model.offre.Offre;

public interface OffreStorage {
    Offre save(Offre offre);
}
