package com.ynov.infra.controller.exception;

import com.ynov.domain.model.exception.EntrepriseAlreadyExistingException;
import com.ynov.domain.model.exception.EntrepriseNotFoundException;
import com.ynov.domain.model.exception.ProfilAlreadyExistingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.Response.Status.*;

@Provider
public class CommandExceptionHandler implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception exception) {
        Status status = INTERNAL_SERVER_ERROR;

        if (exception instanceof EntrepriseAlreadyExistingException
            || exception instanceof ProfilAlreadyExistingException) {
            status = BAD_REQUEST;
        }

        if (exception instanceof EntrepriseNotFoundException) {
            status = NOT_FOUND;
        }

        return Response.status(status).entity(exception.getMessage()).build();
    }
}
