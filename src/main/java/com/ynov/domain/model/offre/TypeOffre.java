package com.ynov.domain.model.offre;

public enum TypeOffre {
    CDI,
    CDD,
    PRESTATION
}
