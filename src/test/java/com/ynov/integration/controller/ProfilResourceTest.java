package com.ynov.integration.controller;

import com.ynov.infra.controller.dto.request.MotCleRequestDto;
import com.ynov.infra.controller.dto.request.offre.CreateOffreRequestDto;
import com.ynov.infra.controller.dto.request.profil.CreateProfilRequestDto;
import com.ynov.infra.repository.ProfilRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

import static com.ynov.domain.model.offre.TypeOffre.CDI;
import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
public class ProfilResourceTest {
    @Inject
    ProfilRepository profilRepository;

    @BeforeEach
    @Transactional
    public void setup(){
        profilRepository.deleteAll();
    }

    @Test
    public void createProfil_shoudReturnId(){
        var createProfilRequestDto = new CreateProfilRequestDto("jeanne", "boulot", "photo", "jb@test.com", "", List.of(new MotCleRequestDto("JAVA")), 15, CDI);

        given().contentType("application/json")
                .body(createProfilRequestDto)
                .when()
                .post("/api/profils")
                .then()
                .statusCode(CREATED.getStatusCode())
                .body("id", is(notNullValue()));
    }

    @Test
    public void createTwoProfile_withTheSameEmail_shouldReturnAnError400(){
        var createProfilRequestDto = new CreateProfilRequestDto("jeanne", "boulot", "photo", "jb@test.com", "", List.of(new MotCleRequestDto("JAVA")), 15, CDI);
        var createProfilRequestDto2 = new CreateProfilRequestDto("jeanne", "boulot", "photo", "jb@test.com", "", List.of(new MotCleRequestDto("JAVA")), 15, CDI);

        given().contentType("application/json")
                .body(createProfilRequestDto)
                .when()
                .post("/api/profils");

        given().contentType("application/json")
                .body(createProfilRequestDto2)
                .when()
                .post("/api/profils")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createProfil_withEmptyEmail_shouldReturnAnError400(){
        var createProfilRequestDto = new CreateProfilRequestDto("jeanne", "boulot", "photo", "", "", List.of(new MotCleRequestDto("JAVA")), 15, CDI);

        given().contentType("application/json")
                .body(createProfilRequestDto)
                .when()
                .post("/api/profils")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createProfil_withEmptyFirstname_shouldReturnAnError400(){
        var createProfilRequestDto = new CreateProfilRequestDto("", "boulot", "photo", "email@mail.com", "", List.of(new MotCleRequestDto("JAVA")), 15, CDI);

        given().contentType("application/json")
                .body(createProfilRequestDto)
                .when()
                .post("/api/profils")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createProfil_withEmptyLastname_shouldReturnAnError400(){
        var createProfilRequestDto = new CreateProfilRequestDto("firstname", "", "photo", "email@email.com", "", List.of(new MotCleRequestDto("JAVA")), 15, CDI);

        given().contentType("application/json")
                .body(createProfilRequestDto)
                .when()
                .post("/api/profils")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }
}
