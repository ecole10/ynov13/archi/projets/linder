package com.ynov.domain.port.input.profil;

import com.ynov.domain.model.Profil;
import com.ynov.domain.model.exception.ProfilAlreadyExistingException;
import com.ynov.domain.port.output.ProfilStorage;

import javax.persistence.PersistenceException;

public class CreateProfil {

    private final ProfilStorage profilStorage;

    public CreateProfil(ProfilStorage profilStorage) {
        this.profilStorage = profilStorage;
    }

    public Profil create(Profil newProfil) {
        try {
            return profilStorage.save(newProfil);
        } catch (PersistenceException e){
            throw new ProfilAlreadyExistingException();
        }
    }
}
