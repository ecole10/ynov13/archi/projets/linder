package com.ynov.infra.repository.dao;

import com.ynov.infra.repository.dao.offre.OffreDao;
import com.ynov.infra.repository.dao.profil.ProfilDao;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "mot_cle")
@Getter
@Setter
@NoArgsConstructor
public class MotCleDao {
    @Id
    private String valeur;

    @ManyToMany
    @JoinTable(
            name = "offre_mot_cle",
            joinColumns = @JoinColumn(name = "mot_cle_valeur"),
            inverseJoinColumns = @JoinColumn(name = "offre_id")
    )
    private List<OffreDao> offres;

    @ManyToMany
    @JoinTable(
            name = "profil_mot_cle",
            joinColumns = @JoinColumn(name = "mot_cle_valeur"),
            inverseJoinColumns = @JoinColumn(name = "profil_id")
    )
    private List<ProfilDao> profils;
}
