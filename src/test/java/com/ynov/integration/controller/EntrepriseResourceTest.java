package com.ynov.integration.controller;

import com.ynov.infra.controller.dto.request.entreprise.CreateEntrepriseRequestDto;
import com.ynov.infra.repository.EntepriseRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class EntrepriseResourceTest {
    @Inject
    EntepriseRepository entepriseRepository;

    @BeforeEach
    @Transactional
    public void setup(){
        entepriseRepository.deleteAll();
    }

    @Test
    public void createEntreprise_shoudReturnSiren(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "123456789", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(CREATED.getStatusCode())
                .body("siren", is("123456789"));
    }

    @Test
    public void createEntreprise_withEmptyDescription_shoudReturnSiren(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "123456789", "", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(CREATED.getStatusCode())
                .body("siren", is("123456789"));
    }

    @Test
    public void createEntreprise_withEmptyName_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("", "123456789", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createEntreprise_withEmptySiren_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createEntreprise_withSirenLowerThan9Digits_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "12345678", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createEntreprise_withSirenGreaterThan9Digits_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "1234567891", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createEntreprise_withSirenNotDigits_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "sirenfake", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createEntreprise_withDescriptionGreaterThan280Characters_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "123456789", "description".repeat(30), "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createEntreprise_withEmptyLogo_shouldReturnAnError400(){
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "123456789", "description", "");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createTwoEntreprise_withTheSameSiren_shouldReturnAnError400() {
        var createEntrepriseRequestDto = new CreateEntrepriseRequestDto("entreprise", "123456789", "description", "logo");
        var createEntrepriseRequestDto2 = new CreateEntrepriseRequestDto("entreprise", "123456789", "description", "logo");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto)
                .when()
                .post("/api/entreprises");

        given().contentType("application/json")
                .body(createEntrepriseRequestDto2)
                .when()
                .post("/api/entreprises")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }
}
