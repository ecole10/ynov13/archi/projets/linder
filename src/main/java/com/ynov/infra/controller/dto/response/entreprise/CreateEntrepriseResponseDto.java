package com.ynov.infra.controller.dto.response.entreprise;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateEntrepriseResponseDto {
    private String siren;
}
