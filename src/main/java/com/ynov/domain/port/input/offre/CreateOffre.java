package com.ynov.domain.port.input.offre;

import com.ynov.domain.model.offre.Offre;
import com.ynov.domain.model.exception.EntrepriseNotFoundException;
import com.ynov.domain.port.output.OffreStorage;

public class CreateOffre {
    private final OffreStorage offreStorage;

    public CreateOffre(OffreStorage offreStorage) {
        this.offreStorage = offreStorage;
    }

    public Offre create(Offre newOffre){
        try {
            return offreStorage.save(newOffre);
        } catch (IllegalStateException exception){
            throw new EntrepriseNotFoundException();
        }
    }
}
