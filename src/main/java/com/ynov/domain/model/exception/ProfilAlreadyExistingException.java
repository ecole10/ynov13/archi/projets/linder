package com.ynov.domain.model.exception;

public class ProfilAlreadyExistingException extends RuntimeException{
    public ProfilAlreadyExistingException() {
        super("EMAIL already exists");
    }
}
