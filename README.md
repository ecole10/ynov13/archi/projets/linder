# Linder

[![pipeline status](https://gitlab.com/ynov13/archi/projets/linder/badges/develop/pipeline.svg)](https://gitlab.com/ynov13/archi/projets/linder/-/commits/develop)
[![coverage report](https://gitlab.com/ynov13/archi/projets/linder/badges/develop/coverage.svg)](https://gitlab.com/ynov13/archi/projets/linder/-/commits/develop)
[![Latest Release](https://gitlab.com/ynov13/archi/projets/linder/-/badges/release.svg)](https://gitlab.com/ynov13/archi/projets/linder/-/releases)

## Liens utiles

- [Domain Storytelling](https://miro.com/app/board/uXjVOCNDv2M=/?invite_link_id=729867030134)
- [Swagger](https://ynov-linder-staging.herokuapp.com/swagger/)
- [Jacoco report](https://ynov13.gitlab.io/archi/projets/linder/)
