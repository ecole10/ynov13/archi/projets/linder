package com.ynov.domain.port.input.entreprise;

import com.ynov.domain.model.Entreprise;
import com.ynov.domain.model.exception.EntrepriseAlreadyExistingException;
import com.ynov.domain.port.output.EntrepriseStorage;

import javax.persistence.PersistenceException;

public class CreateEnterprise {
    private final EntrepriseStorage entrepriseStorage;

    public CreateEnterprise(EntrepriseStorage entrepriseStorage) {
        this.entrepriseStorage = entrepriseStorage;
    }

    public Entreprise create(Entreprise newEntreprise) {
        try {
            return entrepriseStorage.save(newEntreprise);
        } catch (PersistenceException e){
            throw new EntrepriseAlreadyExistingException();
        }
    }
}
