package com.ynov.domain.model.exception;

public class EntrepriseNotFoundException extends RuntimeException {
    public EntrepriseNotFoundException() {
        super("SIREN doesn't exists");
    }
}
