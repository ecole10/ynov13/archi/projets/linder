package com.ynov.domain.port.output;

import com.ynov.domain.model.Profil;

public interface ProfilStorage {
    Profil save(Profil profil);
}
