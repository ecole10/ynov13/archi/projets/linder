package com.ynov.domain.model.exception;

public class EntrepriseAlreadyExistingException extends RuntimeException{
    public EntrepriseAlreadyExistingException() {
        super("SIREN already exists");
    }
}
