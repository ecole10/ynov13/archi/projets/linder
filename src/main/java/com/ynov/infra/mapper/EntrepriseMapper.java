package com.ynov.infra.mapper;

import com.ynov.domain.model.Entreprise;
import com.ynov.infra.controller.dto.request.entreprise.CreateEntrepriseRequestDto;
import com.ynov.infra.controller.dto.response.entreprise.CreateEntrepriseResponseDto;
import com.ynov.infra.repository.dao.entreprise.EntrepriseDao;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "cdi")
public interface EntrepriseMapper {
    @Mapping(target = "dateInscription", ignore = true)
    Entreprise createEntrepriseRequestDtoToEntreprise(CreateEntrepriseRequestDto createEntrepriseRequestDto);

    EntrepriseDao entrepriseToEntrepriseDao(Entreprise entreprise);
    Entreprise entrepriseDaoToEntreprise(EntrepriseDao entrepriseDao);

    CreateEntrepriseResponseDto entrepriseToCreateEntrepriseResponseDto(Entreprise entreprise);
}
