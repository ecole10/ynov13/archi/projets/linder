package com.ynov.architecture;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

@AnalyzeClasses(packages = "com.ynov")
public class ArchDomainIsolationTests {

    @ArchTest
    static final ArchRule domain_shouldKnowNothingOfInfrastructure =
            ArchRuleDefinition.noClasses().that().resideInAPackage("..domain..")
                    .should().accessClassesThat().resideInAPackage("..infra..");
}
