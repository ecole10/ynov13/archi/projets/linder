package com.ynov.infra.controller;

import com.ynov.domain.model.offre.Offre;
import com.ynov.domain.port.input.offre.CreateOffre;
import com.ynov.domain.port.output.OffreStorage;
import com.ynov.infra.controller.dto.request.offre.CreateOffreRequestDto;
import com.ynov.infra.controller.dto.response.offre.CreateOffreResponseDto;
import com.ynov.infra.mapper.OffreMapper;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.CREATED;

@Tag(name = "Offre")
@Path("offres")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class OffreResource {
    @Inject
    OffreMapper offreMapper;

    @Inject
    OffreStorage offreStorage;

    @POST
    @APIResponses(value = {
            @APIResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = CreateOffreResponseDto.class))),
            @APIResponse(responseCode = "400", content = @Content(example = "SIREN can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "A valid SIREN is composed of 9 digits")),
            @APIResponse(responseCode = "400", content = @Content(example = "Description max size is 280 characters")),
            @APIResponse(responseCode = "404", content = @Content(example = "SIREN doesn't exists")),
    })
    public Response createOffre(@Valid CreateOffreRequestDto request) {
        CreateOffre createOffre = new CreateOffre(offreStorage);

        Offre newOffre = offreMapper.createOffreRequestDtoToOffre(request);
        Offre offre = createOffre.create(newOffre);

        return Response.status(CREATED)
                .entity(offreMapper.OffreToCreateOffreResponseDto(offre))
                .build();
    }
}
