package com.ynov.infra.mapper;

import com.ynov.domain.model.Profil;
import com.ynov.infra.controller.dto.request.profil.CreateProfilRequestDto;
import com.ynov.infra.controller.dto.response.profil.CreateProfilResponseDto;
import com.ynov.infra.repository.dao.profil.ProfilDao;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "cdi", uses = MotCleMapper.class)
public interface ProfilMapper {
    @Mapping(target = "id", ignore = true)
    Profil createProfilRequestDtoToProfil(CreateProfilRequestDto createProfilRequestDto);

    @Mapping(target = "id", ignore = true)
    ProfilDao profilToProfilDao(Profil profil);
    @Mapping(expression = "java(profilDao.getId().toString())", target = "id")
    Profil profilDaoToProfil(ProfilDao profilDao);

    CreateProfilResponseDto profilToCreateprofilResponseDto(Profil profil);
}
