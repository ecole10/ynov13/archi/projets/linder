package com.ynov.infra.controller.dto.request.offre;

import com.ynov.domain.model.offre.StatutOffre;
import com.ynov.domain.model.offre.TypeOffre;
import com.ynov.infra.controller.dto.request.MotCleRequestDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;
import javax.validation.constraints.*;

import java.util.List;

import static javax.persistence.EnumType.STRING;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateOffreRequestDto {
    @NotEmpty(message = "SIREN can't be empty")
    @Digits(integer = 9, fraction = 0)
    @Size(min = 9, max = 9, message = "A valid SIREN is composed of 9 digits")
    private String siren;

    @Size(max = 280, message = "Description max size is 280 characters")
    private String description;

    @DecimalMin("8.37")
    private double salaire;

    private List<MotCleRequestDto> motsCles;

    @NotNull
    @Enumerated(STRING)
    private TypeOffre type;

    @NotNull
    @Enumerated(STRING)
    private StatutOffre statut;

    private boolean accessibleRecruteur;
}
