package com.ynov.integration.controller;

import com.ynov.infra.controller.dto.request.MotCleRequestDto;
import com.ynov.infra.controller.dto.request.offre.CreateOffreRequestDto;
import com.ynov.infra.controller.dto.request.entreprise.CreateEntrepriseRequestDto;
import com.ynov.infra.repository.EntepriseRepository;
import com.ynov.infra.repository.OffreRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.List;

import static com.ynov.domain.model.offre.StatutOffre.OUVERT;
import static com.ynov.domain.model.offre.TypeOffre.CDI;
import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
public class OffreResourceTest {
    @Inject
    OffreRepository offreRepository;

    @Inject
    EntepriseRepository entepriseRepository;

    @BeforeEach
    @Transactional
    public void setup(){
        offreRepository.deleteAll();
        entepriseRepository.deleteAll();
    }

    @Test
    public void createOffre_shouldReturnAnId(){
        var createEntrepriseRequestDto1 = new CreateEntrepriseRequestDto("nameEntrepriseTest", "123456789", "noDescription", "urlLogo");
        given().contentType("application/json")
                .body(createEntrepriseRequestDto1)
                .when()
                .post("/api/entreprises");


        CreateOffreRequestDto createOffreRequestDto = new CreateOffreRequestDto("123456789","description test", 9.37, List.of(new MotCleRequestDto("JAVA")), CDI, OUVERT ,false);
        given().contentType("application/json")
                .body(createOffreRequestDto)
                .when()
                .post("/api/offres")
                .then()
                .statusCode(CREATED.getStatusCode())
                .body("id", is(notNullValue()));
    }

    @Test
    public void createOffre_withEmptySiren_shouldReturnAnError400(){
        CreateOffreRequestDto createOffreRequestDto = new CreateOffreRequestDto("","description test", 9.37, List.of(new MotCleRequestDto("JAVA")), CDI, OUVERT ,false);
        given().contentType("application/json")
                .body(createOffreRequestDto)
                .when()
                .post("/api/offres")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void createOffre_withUnknowEntreprise_shouldReturnAnError404(){
        CreateOffreRequestDto createOffreRequestDto = new CreateOffreRequestDto("123456789","description test", 9.37, List.of(new MotCleRequestDto("JAVA")), CDI, OUVERT ,false);
        given().contentType("application/json")
                .body(createOffreRequestDto)
                .when()
                .post("/api/offres")
                .then()
                .statusCode(NOT_FOUND.getStatusCode());
    }
}
