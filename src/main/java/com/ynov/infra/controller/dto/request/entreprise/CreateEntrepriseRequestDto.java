package com.ynov.infra.controller.dto.request.entreprise;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateEntrepriseRequestDto {
    @NotEmpty(message = "Name can't be empty")
    private String nom;

    @NotEmpty(message = "SIREN can't be empty")
    @Digits(integer = 9, fraction = 0)
    @Size(min = 9, max = 9, message = "A valid SIREN is composed of 9 digits")
    private String siren;

    @Size(max = 280, message = "Description max size is 280 characters")
    private String description;

    @NotEmpty(message = "Logo url can't be empty")
    private String logo;
}
