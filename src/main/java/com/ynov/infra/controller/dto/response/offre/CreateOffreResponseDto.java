package com.ynov.infra.controller.dto.response.offre;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateOffreResponseDto {
    private String id;
}
