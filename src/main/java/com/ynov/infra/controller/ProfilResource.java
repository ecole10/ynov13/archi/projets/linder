package com.ynov.infra.controller;

import com.ynov.domain.model.Profil;
import com.ynov.domain.port.input.profil.CreateProfil;
import com.ynov.domain.port.output.ProfilStorage;
import com.ynov.infra.controller.dto.request.profil.CreateProfilRequestDto;
import com.ynov.infra.controller.dto.response.profil.CreateProfilResponseDto;
import com.ynov.infra.mapper.ProfilMapper;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.CREATED;

@Tag(name = "Profil")
@Path("profils")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProfilResource {
    @Inject
    ProfilMapper profilMapper;

    @Inject
    ProfilStorage profilStorage;

    @POST
    @APIResponses(value = {
            @APIResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = CreateProfilResponseDto.class))),
            @APIResponse(responseCode = "400", content = @Content(example = "Firstname can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "Lastname can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "Picture can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "Email can't be empty")),
            @APIResponse(responseCode = "400", content = @Content(example = "EMAIL already exists")),
            @APIResponse(responseCode = "400", content = @Content(example = "Description max size is 280 characters")),
    })
    public Response createProfil(@Valid CreateProfilRequestDto request) {
        CreateProfil createProfil = new CreateProfil(profilStorage);

        Profil newProfil = profilMapper.createProfilRequestDtoToProfil(request);
        Profil profil = createProfil.create(newProfil);

        return Response.status(CREATED)
                .entity(profilMapper.profilToCreateprofilResponseDto(profil))
                .build();
    }
}
