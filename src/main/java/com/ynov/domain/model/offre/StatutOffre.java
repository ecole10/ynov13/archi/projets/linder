package com.ynov.domain.model.offre;

public enum StatutOffre {
    OUVERT,
    POURVUE,
    FERMEE
}
