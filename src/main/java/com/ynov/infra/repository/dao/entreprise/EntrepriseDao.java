package com.ynov.infra.repository.dao.entreprise;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "entreprise")
@Getter
@Setter
@NoArgsConstructor
public class EntrepriseDao {
    @Id
    private String siren;

    @Column(name = "nom")
    private String nom;

    @Length(max = 280)
    @Column(name = "description")
    private String description;

    @Column(name = "logo")
    private String logo;

    @CreationTimestamp
    @Column(name = "date_inscription")
    private Date dateInscription;
}
