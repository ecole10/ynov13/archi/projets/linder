package com.ynov.infra.mapper;

import com.ynov.domain.model.offre.Offre;
import com.ynov.infra.controller.dto.request.offre.CreateOffreRequestDto;
import com.ynov.infra.controller.dto.response.offre.CreateOffreResponseDto;
import com.ynov.infra.repository.dao.offre.OffreDao;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "cdi", uses = MotCleMapper.class)
public interface OffreMapper {
    @Mapping(target = "id", ignore = true)
    Offre createOffreRequestDtoToOffre(CreateOffreRequestDto offre);

    CreateOffreResponseDto OffreToCreateOffreResponseDto(Offre offre);

    @Mapping(source = "siren", target = "entreprise.siren")
    @Mapping(target = "id", ignore = true)
    OffreDao OffreToOffreDao(Offre offre);

    @Mapping(expression = "java(offreDao.getEntreprise().getSiren())", target = "siren")
    @Mapping(expression = "java(offreDao.getId().toString())", target = "id")
    Offre OffreDaoToOffre(OffreDao offreDao);
}
