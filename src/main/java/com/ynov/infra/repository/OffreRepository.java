package com.ynov.infra.repository;

import com.ynov.domain.model.offre.Offre;
import com.ynov.domain.port.output.OffreStorage;
import com.ynov.infra.mapper.OffreMapper;
import com.ynov.infra.repository.dao.MotCleDao;
import com.ynov.infra.repository.dao.offre.OffreDao;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class OffreRepository implements OffreStorage, PanacheRepositoryBase<OffreDao, String> {
    @Inject
    OffreMapper offreMapper;

    @Inject
    MotCleRepository motCleRepository;

    @Override
    @Transactional
    public Offre save(Offre offre) {
        OffreDao offreDao = offreMapper.OffreToOffreDao(offre);

        for (MotCleDao motCleDao : offreDao.getMotsCles()) {
            motCleRepository.save(motCleDao);
        }

        persistAndFlush(offreDao);
        return offreMapper.OffreDaoToOffre(offreDao);
    }
}
