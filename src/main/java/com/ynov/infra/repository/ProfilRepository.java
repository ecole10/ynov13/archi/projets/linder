package com.ynov.infra.repository;

import com.ynov.domain.model.Profil;
import com.ynov.domain.port.output.ProfilStorage;
import com.ynov.infra.mapper.ProfilMapper;
import com.ynov.infra.repository.dao.MotCleDao;
import com.ynov.infra.repository.dao.profil.ProfilDao;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class ProfilRepository implements ProfilStorage, PanacheRepositoryBase<ProfilDao, String> {
    @Inject
    ProfilMapper profilMapper;

    @Inject
    MotCleRepository motCleRepository;

    @Override
    @Transactional
    public Profil save(Profil profil) {
        ProfilDao profilDao = profilMapper.profilToProfilDao(profil);

        for (MotCleDao motCleDao : profilDao.getMotsCles()) {
            motCleRepository.save(motCleDao);
        }

        persistAndFlush(profilDao);
        return profilMapper.profilDaoToProfil(profilDao);
    }
}
