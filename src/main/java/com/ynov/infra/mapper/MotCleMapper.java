package com.ynov.infra.mapper;

import com.ynov.domain.model.MotCle;
import com.ynov.infra.controller.dto.request.MotCleRequestDto;
import com.ynov.infra.repository.dao.MotCleDao;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "cdi")
public interface MotCleMapper {
    @Mapping(expression = "java(motCleRequestDto.getValeur().toUpperCase())", target = "valeur")
    MotCle motCleRequestDtoToMotCle(MotCleRequestDto motCleRequestDto);
    Set<MotCle> motCleRequestDtosToMotCles(Set<MotCleRequestDto> motCleRequestDtos);

    MotCleDao motCleToMotCleDao(MotCle motCle);
    Set<MotCleDao> motClesToMotCleDaos(Set<MotCle> motCles);

    MotCle motCleDaoToMotCle(MotCleDao motCleDao);
    Set<MotCle> motCleDaosToMotCles(Set<MotCleDao> motCleDaos);
}
