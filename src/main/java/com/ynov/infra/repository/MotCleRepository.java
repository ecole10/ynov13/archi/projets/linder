package com.ynov.infra.repository;

import com.ynov.infra.repository.dao.MotCleDao;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
public class MotCleRepository implements PanacheRepositoryBase<MotCleDao, String> {

    @Transactional
    public void save(MotCleDao motCleDao) {
        if (!exist(motCleDao.getValeur())) {
            persistAndFlush(motCleDao);
        }
    }

    private boolean exist(String valeur) {
        return findByIdOptional(valeur).isPresent();
    }
}
