package com.ynov.domain.model.offre;

import com.ynov.domain.model.MotCle;

import java.util.List;

public record Offre(String id, String description, double salaire, List<MotCle> motsCles, TypeOffre type, StatutOffre statut, boolean accessibleRecruteur, String siren){

}
