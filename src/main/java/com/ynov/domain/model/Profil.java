package com.ynov.domain.model;

import com.ynov.domain.model.offre.TypeOffre;

import java.util.List;

public record Profil(String id, String prenom, String nom, String email, String photo, String description, List<MotCle> motsCles, double salaireHoraireNetMinimum, TypeOffre offre) {

}
