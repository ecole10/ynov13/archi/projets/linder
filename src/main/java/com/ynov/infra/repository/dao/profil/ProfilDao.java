package com.ynov.infra.repository.dao.profil;

import com.ynov.domain.model.offre.TypeOffre;
import com.ynov.infra.repository.dao.MotCleDao;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;

@Entity(name = "profil")
@Getter
@Setter
@NoArgsConstructor
public class ProfilDao {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Column(name = "photo")
    private String photo;

    @Length(max = 280)
    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(
            name = "profil_mot_cle",
            joinColumns = @JoinColumn(name = "profil_id"),
            inverseJoinColumns = @JoinColumn(name = "mot_cle_valeur")
    )
    private List<MotCleDao> motsCles;

    @Column(name = "salaire_horaire_net_minimum")
    private double salaireHoraireNetMinimum;

    @Enumerated(STRING)
    @Column(name = "offre")
    private TypeOffre offre;
}
